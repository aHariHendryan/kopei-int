<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('menu_helper'))
{

    function menu_user($login)
    {
        $menu = '<ul class="afterlogin-menu">';

        if ($login == 0) { // tampilkan buyer menu
            $menu .= '
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'user/my_order">MY ORDERS</a></li>
            ';
        } else if ($login == 1) { // tampilkan dealer menu
            $menu .= '
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'dealer/all_orders_latest">ACTIVE ORDER</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'dealer/all_orders_spk">SPK ORDER</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'dealer/all_orders_review">REVIEW ORDER</a></li>
            ';
        } else if ($login == 2) { // tampilkan admin menu
            $menu .= '
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'admin/admin_control">ADMIN CONTROL</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'admin/all_orders_latest">BERI PENAWARAN</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'admin/follow_up_order_new">FOLLOW UP ORDER</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'admin/transaction">TRANSACTION</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'admin/all_approved_order">ALL APPROVED ORDER</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'admin/user_page">USER PAGE</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'master_crud/dealer_all">DEALER PAGE</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'admin/sales_page">SALES PAGE</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'master_crud/car_brand_all">MASTER CAR BRAND</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'master_crud/car_model_all">MASTER CAR MODEL</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'master_crud/car_type_all">MASTER CAR TYPE</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'master_crud/insurance_all">MASTER INSURANCE</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'master_crud/location_city_all">MASTER CITY</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'master_crud/leasing_all">MASTER LEASING</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'master_crud/setting_system_all">MASTER SETTING SYSTEM</a></li>
                <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'master_crud/transaction_code_all">MASTER TRANSACTION CODE</a></li>                                
                <ul class="afterlogin-menu">
                
                </ul>
            ';
        } else {
            redirect('/mulmut/logout');
        }

        $menu .= '
            <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'mulmut/my_account">ACCOUNT SETTINGS</a></li>
            <li class="afterlogin-menu-content"><a class="" href="' . base_url() . 'mulmut/logout">LOGOUT</a></li>
        </ul>';

        return $menu;
    }

}