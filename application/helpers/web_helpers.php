<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('custom_helper'))
{
    function __construct()
    {
        parent::__construct();
        
    }

    function mandrill_email($from, $fromname, $to, $message, $subject, $bcc='')
    {
    	$CI = get_instance();

        $CI->load->config('mandrill');

        $CI->load->library('mandrill');
    	
        $mandrill_ready = NULL;

        try {
            $CI->mandrill->init(MANDRILL_API_KEY);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }

        if ($mandrill_ready) {

            if($bcc == ''){
                $email = array(
                    'html' => '<p>' . $message . '<p>', //Consider using a view file
                    'text' => $message,
                    'subject' => $subject,
                    'from_email' => $from,
                    'from_name' => $fromname,
                    'to' => array(array('email' => $to)) //Check documentation for more details on this one
                );
            }else{
                $email = array(
                    'html' => '<p>' . $message . '<p>', //Consider using a view file
                    'text' => $message,
                    'subject' => $subject,
                    'from_email' => $from,
                    'from_name' => $fromname,
                    'to' => array(array('email' => $to)), //Check documentation for more details on this one
                    'bcc' => array(array('email' => $bcc))
                    //'to' => array(array('email' => 'joe@example.com' ),array('email' => 'joe2@example.com' ))
                );
            }

            $result = $CI->mandrill->messages_send($email);

            return $result[0]['reject_reason'];
        }
    }

    
    
}