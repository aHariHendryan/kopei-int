<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('custom_helper'))
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('authenticate_m', 'AM');
    }

    function mandrill_email($from, $fromname, $to, $message, $subject, $bcc='')
    {
    	$CI = get_instance();

        $CI->load->config('mandrill');

        $CI->load->library('mandrill');
    	
        $mandrill_ready = NULL;

        try {
            $CI->mandrill->init(MANDRILL_API_KEY);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }

        if ($mandrill_ready) {

            if($bcc == ''){
                $email = array(
                    'html' => '<p>' . $message . '<p>', //Consider using a view file
                    'text' => $message,
                    'subject' => $subject,
                    'from_email' => $from,
                    'from_name' => $fromname,
                    'to' => array(array('email' => $to)) //Check documentation for more details on this one
                );
            }else{
                $email = array(
                    'html' => '<p>' . $message . '<p>', //Consider using a view file
                    'text' => $message,
                    'subject' => $subject,
                    'from_email' => $from,
                    'from_name' => $fromname,
                    'to' => array(array('email' => $to)), //Check documentation for more details on this one
                    'bcc' => array(array('email' => $bcc))
                    //'to' => array(array('email' => 'joe@example.com' ),array('email' => 'joe2@example.com' ))
                );
            }

            $result = $CI->mandrill->messages_send($email);

            return $result[0]['reject_reason'];
        }
    }

    function replaceDot($input)
    {
        return str_replace(".","",$input);
    }

    function replaceSpaceToDash($input)
    {
        return str_replace(" ","-",$input);
    }

    function removeAlpha($input)
    {
        return preg_replace("/[^0-9,.]/", "", $input);
    }
    
    function is_valid_member_seller()
    {
        $CI = get_instance();
        $sess = $CI->session->userdata('datauser');

        //check login
        if(empty($sess)){
            //destroy the session, like logout
            $CI->session->sess_destroy();
            redirect('/');
        }

        $user_data = $CI->ST->get_by_id_single_row('ki_m_user',$sess['id_user'],'id_user');
        
        if($user_data->role == ADMIN){
            redirect('/login');
        }

    }

}