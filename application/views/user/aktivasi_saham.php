            <div class="col-md-9 col-thumbnail">
                <div class="thumbnail thumbnail-dashboard">
                    <div class="row">
                        <div class="col-md-12 text-center" >
                            <h1>Aktivasi Saham</h1><br />
                            Saham anda yang belum aktif : <b><?=$user->inactive_lot?> Point</b><br />
                            Point 1 lembar saham : <b><?=$point_saham?> Point</b>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-offset-2 col-md-10" style="padding: 10px;">
                            <div class="col-md-offset-2 col-md-6 text-center">
                                <form id="saham-form" class="form-horizontal" style="margin: 0 20px">
                                   <div class="form-group">
                                    <label class="col-sm-6 control-label">Jumlah Saham</label>
                                    <div class="col-sm-6">
                                      <input type="number" class="form-control jmlsaham" id="jmlsaham" name="jmlsaham" placeholder="">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-sm-12">
                                      <button type="submit" class="btn btn-default btn-submit">Aktivasi <span class="calculate"></span></button>
                                    </div>
                                  </div>
                                  <div class="show-notifForm"></div>
                                </form>
                           </div> 
                        </div>
                    </div>
                </div>
            </div>
<!--tag open in other page-->
        </div>
    </div>
</section>
<script>
$('.jmlsaham').on('change keyup', function(){
    var curr = $(this).val();
    $('.calculate').html('');
    if(curr != ""){
        calculate = <?=$point_saham?>*curr;
        $('.calculate').html(formatNumber(calculate)+' Point');
    }
});

$('#saham-form').formValidation('destroy').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        jmlsaham: {
            validators: {
                notEmpty: {
                    message: 'Harap diisi!'
                },
                lessThan: {
                    value: <?=$max_activate_saham?>,
                    message: 'Maksimal '+<?=$max_activate_saham?>+' lembar'
                },
                greaterThan: {
                    value: 1,
                    message: 'Minimal 1 lembar'
                }
            }
        },
    }
}).on('success.form.fv', function (e) {
    e.preventDefault();
    var dataString = $("#saham-form").serialize();
    $(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
    $(".btn-submit").attr("disabled", 'disabled');
    $.ajax({
    url: '<?=base_url()?>user/do_aktivasi_saham',
    type: 'POST',
    data: dataString,
    success: function(data) {
      //var obj = jQuery.parseJSON(data);
//      if (obj.hasOwnProperty("false")) {
//        $(".btn-submit").html('Aktivasi');
//        $(".btn-submit").removeAttr("disabled");
//        $('#signup-form .show-notifForm' ).empty();
//        $('#signup-form .show-notifForm' ).append( '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button><strong>Oops! </strong>'+obj.false+'</div>' );
//      } else {
        window.location = '<?=base_url()?>aktivasi-saham';
      //}
    }
    });
    return false;
}).on('err.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(true);
    }
}).on('success.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(false);
    }
});
</script>