            <div class="col-md-9 thumbnail" style="margin-left: -15px;">
                <div class="col-md-12 text-center" >
                    <h1>Beli Saham</h1><br />
                    Harga 1 lembar saham : <b>Rp. <?=number_format($harga_saham,'0','','.')?></b>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-offset-2 col-md-10" style="padding: 10px;">
                    <div class="col-md-offset-2 col-md-6 text-center">
                        <form id="saham-form" class="form-horizontal" style="margin: 0 20px">
                           <div class="form-group">
                            <label class="col-sm-6 control-label">Jumlah Saham</label>
                            <div class="col-sm-6">
                              <input style="width:80%" type="number" class="form-control jmlsaham" id="jmlsaham" name="jmlsaham" placeholder="">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-12">
                              <button type="submit" class="btn btn-default btn-submit">Bayar <span class="calculate"></span></button>
                            </div>
                          </div>
                          <div class="show-notifForm"></div>
                        </form>
                   </div> 
                </div>
            </div>
<!--tag open in other page-->
        </div>
    </div>
</section>
<script>
$('.jmlsaham').on('change keyup', function(){
    var curr = $(this).val();
    $('.calculate').html('');
    if(curr != ""){
        calculate = <?=$harga_saham?>*curr;
        $('.calculate').html('Rp '+formatNumber(calculate));
    }
});

$('#saham-form').formValidation('destroy').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        jmlsaham: {
            validators: {
                notEmpty: {
                    message: 'Harap diisi!'
                },
                lessThan: {
                    value: 20,
                    message: 'Maksimal 20 lembar'
                }
            }
        },
    }
}).on('success.form.fv', function (e) {
    e.preventDefault();
    var dataString = $("#saham-form").serialize();
    $(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
    $(".btn-submit").attr("disabled", 'disabled');
    $.ajax({
    url: '<?=base_url()?>user/do_saham',
    type: 'POST',
    data: dataString,
    success: function(data) {
      var obj = jQuery.parseJSON(data);
      if (obj.hasOwnProperty("false")) {
        $(".btn-submit").html('Register');
        $(".btn-submit").removeAttr("disabled");
        $('#signup-form .show-notifForm' ).empty();
        $('#signup-form .show-notifForm' ).append( '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button><strong>Oops! </strong>'+obj.false+'</div>' );
      } else {
        window.location = '<?=base_url()?>user-dashboard';
      }
    }
    });
    return false;
}).on('err.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(true);
    }
}).on('success.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(false);
    }
});
</script>