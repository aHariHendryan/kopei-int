<section class="grey-gradation-bg text-center contact-wrap" style="font: sans-serif;" id="contact">
    <div class="container">
        <div class="col-md-12 footer-top-content">
            <div class="row">
                <div class="col-md-offset-1 col-md-3">
                    <ul class="list-static-page">
                        <li>
                            <a href="<?php echo base_url(); ?>">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>">FAQ</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>">About Us</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>">Contact Us</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-contact">
                        <li>
                            <p class="footer-legals">Copyright &copy; 2016 Kopei-int<br/>All rights reserved</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-contact">
                        <li>
                            <p class="footer-legals"><i class="fa fa-envelope"></i> <a href="mailto:info@kopei-int.com?Subject=Hello" target="_top">info@kopei-int.com</a><br />
                            <i class="fa fa-phone"></i> 021 123 4567<br />
                            <i class="fa fa-home"></i> Alamat lengkap<br /></p>
                         </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>
<script>

</script>