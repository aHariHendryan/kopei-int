<section class="soft-bg">
<section class="static-content-border" style="padding-top: 70px;">
    <div class="container static-contact-content">
        <div class="col-md-7 contact-us-wrap">
            <div>    
                <p class="static-contact-title">Contact Us</p>
                <p class="static-content-text-body">Alamat Lengkap</p>
                <p class="static-content-text-body"><i class="fa fa-phone"></i> 021 123 4567<br />
                <i class="fa fa-envelope"></i> <a class ="static-contact-email" href="mailto:info@sa-naturalland.com?Subject=Hello" target="_top">info@sa-naturalland.com</a></p>
            </div><br />
            <div id="order-form">
                <p class="static-contact-title">Send Message</p>
                <form name="contact-us-form" id="contact-us-form">
                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                    <div class="form-group">
                        <label>Company Name</label>
                        <input type="text" class="form-control" id="name" name="company_name" />
                    </div>
                    <div class="form-group">
                        <label>Country</label>
                        <input type="text" class="form-control" id="country" name="country" />
                    </div>  
                    <div class="form-group">
                        <label>Your name</label>
                        <input type="email" class="form-control" id="name" name="name" />
                    </div>
                    <div class="form-group">
                        <label>Your email</label>
                        <input type="email" class="form-control" id="email" name="email"/>
                    </div>
                    <div class="form-group">
                        <label>Telephone</label>
                        <input type="email" class="form-control" id="telephone" name="telephone" />
                    </div>
                    <div class="form-group">
                        <label>Subject</label>
                        <input type="email" class="form-control" id="subject" name="subject" />
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <textarea rows="5" class="form-control" id="message" name="message"></textarea>
                    </div>
                    <div class="form-group contact-us-group">
                        <button type="submit" class="contact-us-tag btn btn-info" style="margin: 10px 0 0 0" >Kirim Pesan</button>
                    </div>
                    <div class="show-notifForm"></div>
                </form> <br />
            </div>
        </div>
        <div class="col-md-4 col-md-offset-1" style="margin-top:0px"> 
            <span style="font-color:orange!important;border-bottom: 5px orange solid" class="text-center"><h1> Last Products</h1></span>    
                <?php 
                foreach ($last_product as $list) { ?>
                <div class="col-md-12" style="padding: 0 40px 0 40px;">
                    <div>
                        <a class="product-img" href="<?=base_url()?>product-detail?id_product=<?=$list->id?>"><span class="text-center"><h2><strong><?=$list->name?></strong></span></h2></a>
                    </div>
                    <div class="text-center" style="margin-bottom:5px">
                        <a class="product-img" href="<?=base_url()?>product-detail?id_product=<?=$list->id?>"><img class="img-thumbnail" src="<?=base_url().PATH_IMAGE_PRODUCTS.$list->img_path?>"/></a>
                    </div>
                    <div class="text-left" >
                        <span><?=substr($list->description,0,150)?><a href="<?=base_url()?>product-detail?id_product=<?=$list->id?>">....See More</a></span>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php } ?>
        </div>
    </div>
</section>
<section >
    <div class="container static-contact-map">
        <iframe class="hidden-xs" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1983.1373137516248!2d106.79914865791903!3d-6.227477332489619!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNsKwMTMnMzguOSJTIDEwNsKwNDgnMDAuOSJF!5e0!3m2!1sid!2sid!4v1452053385044" 
                width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        <iframe class="hidden-sm hidden-md hidden-lg" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1983.1373137516248!2d106.79914865791903!3d-6.227477332489619!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNsKwMTMnMzguOSJTIDEwNsKwNDgnMDAuOSJF!5e0!3m2!1sid!2sid!4v1452053385044" 
                width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div> 
</section>
</section>

<script type="text/javascript">
    
    $('#contact-us-form').formValidation('destroy').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Email diperlukan'
                        },
                        emailAddress: {
                            message: 'Inputan bukan alamat email yang valid'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Nama diperlukan'
                        }
                    }
                },
                handphone: {
                    validators: {
                        notEmpty: {
                            message: 'No. Handphone diperlukan'
                        }
                    }
                },
                message: {
                    validators: {
                        notEmpty: {
                            message: 'Pesan diperlukan'
                        }
                    }
                },
            }
        }).on('success.form.fv', function (e) {
            e.preventDefault();
            var dataString = $("#contact-us-form").serialize();
            $(".contact-us-tag").html('<i class="fa fa-spinner fa-spin"></i>');
            $(".contact-us-tag").attr("disabled", 'disabled');
            //alert(dataString);
            $.ajax({
            url: '<?=base_url()?>mulmut/contact_us_email',
            type: 'POST',
            data: dataString,
            success: function(data) {
              var obj = jQuery.parseJSON(data);
              if (obj.hasOwnProperty("false")) {
                $(".contact-us-tag").html('Kirim Pesan');
                $(".contact-us-tag").removeAttr("disabled");
                $('#contact-us-form .show-notifForm' ).empty();
                $('#contact-us-form .show-notifForm' ).append( '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops! </strong>'+obj.false+'</div>' );
              } else {
                $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');
                $(".contact-us-tag").hide();
                $('#contact-us-form .show-notifForm').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="success" aria-hidden="true">×</button><strong></strong>' + obj.message + '</div>');
              }
            }
            });
            return false;
        }).on('err.field.fv', function (e, data) {
            if (data.fv.getSubmitButton()) {
                data.fv.disableSubmitButtons(true);
            }
        }).on('success.field.fv', function (e, data) {
            if (data.fv.getSubmitButton()) {
                data.fv.disableSubmitButtons(false);
            }
        });
</script>