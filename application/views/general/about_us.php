<section class="static-content-border soft-bg" style="padding-top:40px">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p>
                TLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius tincidunt diam, vel placerat arcu mattis id. Suspendisse volutpat odio non sapien ultricies, eget molestie justo accumsan. Nulla vulputate commodo viverra. Duis ut neque id quam blandit efficitur blandit nec nibh. Cras sit amet consequat turpis. Nunc vel arcu metus. Nulla rutrum auctor placerat. Nam gravida ultricies fermentum. Sed ultricies ipsum non eleifend ullamcorper. Vestibulum bibendum, ante nec interdum auctor, massa dui iaculis turpis, ac suscipit ante dui suscipit augue. Ut dictum bibendum metus, eu dictum mi tempor eu. Ut aliquam euismod justo ut aliquam. Etiam rhoncus nibh at massa tincidunt dictum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam erat volutpat. Vestibulum suscipit nibh at dictum finibus.
        
        Cras vitae purus magna. Vivamus maximus metus vitae facilisis pulvinar. Morbi vehicula tortor imperdiet, lacinia est et, luctus risus. Sed vehicula feugiat pretium. Proin efficitur enim purus, id condimentum ex vestibulum vitae. Quisque sem libero, commodo id mi eget, ullamcorper viverra ex. Vivamus eget cursus arcu. Suspendisse porttitor mattis quam sed fringilla. In eleifend orci ullamcorper lectus commodo pulvinar. Ut eu venenatis sapien. Aenean ullamcorper quis tellus a tristique. Nulla aliquet mauris lacus, in sagittis ligula scelerisque porttitor. Phasellus hendrerit rhoncus elit, vitae venenatis sem feugiat at.
        
        Etiam eu diam in nibh ultricies consequat et vel nisi. In rutrum vitae enim sed cursus. Nullam pharetra vehicula congue. Sed enim arcu, faucibus efficitur augue quis, accumsan euismod felis. Aliquam ut porttitor ex. Aliquam mattis id risus et condimentum. Nunc nec sollicitudin ante, eu consequat nunc. Aliquam ipsum leo, sollicitudin eu porta et, vestibulum quis tellus.
        
        Sed vel iaculis nibh, sed euismod lacus. Etiam non enim nunc. Sed quis odio at tortor faucibus eleifend. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed orci eros, dapibus nec eleifend et, rutrum et nisi. Integer scelerisque leo a urna commodo ornare. Donec auctor velit sed elit ultrices commodo. Vivamus at velit condimentum, rutrum quam sit amet, tristique urna. Curabitur sit amet ante non ligula efficitur interdum eu mollis justo. Aliquam id tempus purus.
        
        Mauris at velit non mauris aliquam luctus eu nec nisl. Nunc et bibendum erat, nec volutpat dui. Vestibulum scelerisque turpis vitae tincidunt fermentum. Sed et neque et libero posuere tincidunt. Sed turpis nisl, lobortis vitae mauris at, gravida venenatis nulla. Integer commodo mattis sodales. Integer vitae pellentesque sapien. Donec congue maximus urna, sed maximus tellus scelerisque ac. Donec laoreet egestas euismod. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut convallis leo vel diam varius ullamcorper.
                </p>
            </div>
            <div class="col-md-4" style="padding-top: 20px;">
            <span style="font-color:orange!important;border-bottom: 5px orange solid" class="text-center"><h1> Last Products</h1></span>    
                <?php 
                foreach ($last_product as $list) { ?>
                <div class="col-md-12" style="padding: 0 40px 0 40px;">
                    <div>
                        <a class="product-img" href="<?=base_url()?>product-detail?id_product=<?=$list->id?>"><span class="text-center"><h2><strong><?=$list->name?></strong></span></h2></a>
                    </div>
                    <div class="text-center" style="margin-bottom:5px">
                        <a class="product-img" href="<?=base_url()?>product-detail?id_product=<?=$list->id?>"><img class="img-thumbnail" src="<?=base_url().PATH_IMAGE_PRODUCTS.$list->img_path?>"/></a>
                    </div>
                    <div class="text-left" >
                        <span><?=substr($list->description,0,150)?><a href="<?=base_url()?>product-detail?id_product=<?=$list->id?>">....See More</a></span>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>