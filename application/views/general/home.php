<!-- START SLIDER -->
<section class="testimony-photo" style="padding-top:53px">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img class="" width="100%" src="<?php echo base_url().PATH_IMAGE_SLIDER; ?>1.jpg" alt="sa-naturalland">
            </div>
            <div class="item">
                <img class="" width="100%" src="<?php echo base_url().PATH_IMAGE_SLIDER; ?>2.jpg" alt="sa-naturalland">
            </div>
            <div class="item">
                <img class="" width="100%" src="<?php echo base_url().PATH_IMAGE_SLIDER; ?>3.jpg" alt="sa-naturalland">
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<!-- END SLIDER -->
<section class="soft-bg">
    <div class="container" >
        <div class="row" style="margin:40px 25px 40px 25px">
            <div class="col-md-12 text-center" style="">
                <h1>Marketing Plan KOPEI-INT</h1><br />
                <span style="font-size: 16px;padding: 20px;">Sistem Marketing Plan yang bertujuan untuk memberikan segudang manfaat bagi seluruh membernya Tanpa terkecuali 
                Didukung dengan perancangan Sistem yang Dinamis dan berkesinambungan jangka Panjang</span>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-dollar fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Total Penghasilan</h2>
                    <h3>Total Akumulasi Sharing Profit Per Slot dengan nilai sharing sebesar 15 Point</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-sitemap fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Bonus Jaringan</h2>
                    <h3>Distribusi 5 Point Bonus Sponsor dan 1 Point Bonus Generasi sampai 10 generasi</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-archive fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Penghasilan Harian</h2>
                    <h3>Bagi hasil yang Adil sesuai tingkat pertumbuhan Slot dan Margin Bisnis Nyata</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-briefcase fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Stok Penghasilan</h2>
                    <h3>Tabungan penghasilan yang menjadi Stok dan pasti dibagikan</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-leaf fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Dana Bersama</h2>
                    <h3>Join 15 Point Per Slot untuk Modal Bisnis Nyata yang akuntabel dan transparan</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-ambulance fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Sistem Auto Generasi</h2>
                    <h3>Sistem Otomatis yang membuat Modal Member terus berputar dalam bisnis</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="light-bg">
    <div class="container" >
        <div class="row" style="margin:40px 25px 40px 25px">
            <div class="col-md-12 text-center" style="">
                <h1>Bisnis Nyata KOPEI-INT</h1><br />
                <span style="font-size: 16px;padding: 20px;">Kami menyediakan Pelayanan Bisnis nyata yang menjadi 
                sebuah Fasilitas Khusus bagi Member ataupun Non Member dalam pemenuhan kebutuhan sehari-hari Full 
                24 Jam Non-Stop</span>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-filter fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>PULSA ELEKTRIK</h2>
                    <h3>Kami menyediakan Fasilitas Pengisian Pulsa semua Nomor All Operator Harga Distributor</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-flash fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>PLN</h2>
                    <h3>Kami Menyediakan Fasilitas Pembayaran PLN Pasca Bayar dan Pra Bayar (TOKEN)</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-laptop fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>TV Berlangganan</h2>
                    <h3>Kami menyediakan fasilitas pembayaran TV Berlangganan Anda dengan respon cepat</h3>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
