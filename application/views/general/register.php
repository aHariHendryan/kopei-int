<div class="container" style="padding-top: 53px;">
    <div class="row">
        <div class="col-md-12 text-center" style="padding: 20px;"><h1>Daftar Member Kopei-int.com</h1></div>
        <div class="clearfix"></div>
        <div class="col-md-offset-2 col-md-6 text-center">
            <form id="signup-form" class="form-horizontal" style="margin: 0 20px">
              <div class="form-group">
                <label class="col-sm-4 control-label">User Sponsor</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="user_sponsor" name="user_sponsor" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Nama Lengkap</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="name" name="name" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Alamat</label>
                <div class="col-sm-8">
                  <textarea class="form-control" name="alamat" rows="3"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Kota</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="kota" name="kota" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Handphone</label>
                <div class="col-sm-8">
                  <input type="number" class="form-control" id="handphone" name="handphone" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Email</label>
                <div class="col-sm-8">
                  <input type="email" class="form-control" id="email" name="email" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Nomor Rekening</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="norekening" name="norekening" placeholder="">
                </div>
              </div>
              <!--div class="form-group">
                <label class="col-sm-4 control-label">Nama Pemegang Rekening</label>
                <div class="col-sm-8">
                  <input type="email" class="form-control" id="nmrekening" name="nmrekening" placeholder="">
                </div>
              </div-->
              <div class="form-group">
                <label class="col-sm-4 control-label">Nama Bank</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="nmbank" name="nmbank" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" id="password" name="password" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Confirm Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Pin</label>
                <div class="col-sm-8">
                  <input type="password" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="6" class="form-control" id="pin" name="pin" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Confirm Pin</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" maxlength="6" id="confirmPin" name="confirmPin" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-10">
                  <button type="submit" class="btn btn-default btn-submit">Register</button>
                </div>
              </div>
              <div class="show-notifForm"></div>
            </form>
       </div> 
    </div>
</div>
<script>

$('#signup-form').formValidation('destroy').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        user_sponsor: {
            validators: {
                notEmpty: {
                    message: 'User sponsor diperlukan'
                },
                remote: {
                    message: 'User sponsor tidak terdaftar',
                    url: '<?=base_url()?>general/check_sponsor',
                    type: 'POST',
                    delay: 1200,
                }
            },
        },
        name: {
            validators: {
                notEmpty: {
                    message: 'Nama lengkap diperlukan'
                }
            }
        },
        kota: {
            validators: {
                notEmpty: {
                    message: 'Kota diperlukan'
                }
            }
        },
        handphone: {
            validators: {
                notEmpty: {
                    message: 'Handphone diperlukan'
                },
            }
        },
        email: {
            validators: {
                notEmpty: {
                    message: 'Email diperlukan'
                },
                emailAddress: {
                    message: 'Inputan bukan alamat email yang valid'
                },
                remote: {
                            message: 'Email sudah terdaftar',
                            url: '<?=base_url()?>general/check_email',
                            type: 'POST',
                            delay: 1200,
                        }
            }
        },
        norekening: {
            validators: {
                notEmpty: {
                    message: 'Nomor Rekening diperlukan'
                },
            }
        },
        nmbank: {
            validators: {
                notEmpty: {
                    message: 'Nama Bank diperlukan'
                },
            }
        },
        password: {
            validators: {
                notEmpty: {
                    message: 'Password diperlukan'
                },
                //identical: {
//                    field: 'confirmPassword',
//                    message: 'Password dan konfirmasi password tidak sama'
//                }
            }
        },
        confirmPassword: {
            validators: {
                //notEmpty: {
//                    message: 'Konfirmasi password diperlukan'
//                },
                identical: {
                    field: 'password',
                    message: 'Password dan konfirmasi password tidak sama'
                }
            }
        },
        pin: {
            validators: {
                notEmpty: {
                    message: 'Pin diperlukan'
                },
                //identical: {
//                    field: 'confirmPin',
//                    message: 'Pin dan konfirmasi Pin tidak sama'
//                },
                integer: {
                    message: 'Harus 6 Digit Angka'
                },
                stringLength: {
                    message: 'Harus 6 Digit Angka',
                    min: function (value, validator, $field) {
                        return 6 - (value.match(/\r/g) || []).length;
                    },
                    max: function (value, validator, $field) {
                        return 6 - (value.match(/\r/g) || []).length;
                    },
                }
            }
        },
        confirmPin: {
            validators: {
                //notEmpty: {
//                    message: 'Konfirmasi pin diperlukan'
//                },
                identical: {
                    field: 'pin',
                    message: 'Pin dan konfirmasi pin tidak sama'
                }
            }
        },
    }
}).on('success.form.fv', function (e) {
    e.preventDefault();
    var dataString = $("#signup-form").serialize();
    $(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
    $(".btn-submit").attr("disabled", 'disabled');
    $.ajax({
    url: '<?=base_url()?>authenticate/signup',
    type: 'POST',
    data: dataString,
    success: function(data) {
      var obj = jQuery.parseJSON(data);
      if (obj.hasOwnProperty("false")) {
        $(".btn-submit").html('Register');
        $(".btn-submit").removeAttr("disabled");
        $('#signup-form .show-notifForm' ).empty();
        $('#signup-form .show-notifForm' ).append( '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops! </strong>'+obj.false+'</div>' );
      } else {
        window.location.href = "<?php echo site_url('authenticate/check_role'); ?>";
      }
    }
    });
    return false;
}).on('err.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(true);
    }
}).on('success.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(false);
    }
});

</script>