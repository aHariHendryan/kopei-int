<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Mulmut.com</title>
<style type="text/css">
body {background-color:#eee;}
.error-template {padding:40px 15px;}
.error-actions {margin-top:15px;margin-bottom:15px;}
.error-actions .btn {margin-right:10px;}
</style>
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet">
</head>
<body>
	<div class="container text-center">
	    <div class="row">
	        <div class="col-md-12" style="padding-top:25px">
	            <div class="error-template text-center">
	            	<img src="<?php echo base_url(); ?>assets/images/logo/mulmut.com.png" style="height:75px;"/>
	            </div>
	            <div>
	            	<h1 style="font-size:12em;margin-bottom:40px;">
	            		:(
	            	</h1>
	                <h1 style="font-size:2em;line-height:150%;margin-bottom:50px">
	                    Situs down karena lagi banyak permintaan mobil.<br/>Sorry ya.
	                </h1>
	                <div class="error-actions">
	                    <a href="<?php echo base_url(); ?>" class="use-btn-orange-large"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;Balik ke Halaman Utama</a>
	                    <!-- <a href="<?php echo base_url(); ?>contact-us" class="use-btn-light-blue-large"><span class="glyphicon glyphicon-envelope"></span>Contact Support</a> -->
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</body>
</html>