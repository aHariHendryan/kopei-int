<section class="grey-gradation-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-left">
                <a href="<?=base_url()?>"><img style="display:inline-block;" src="<?=base_url()?>assets/images/logo.png" alt="" height=""/></a><br />
            </div>
            <div class="col-md-6 text-right" style="margin-top: 6px;">
                <div class="dropdown">
                  <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                   <span class="glyphicon glyphicon-user" aria-hidden="true"></span>  <?=$user->usertologin?>
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="margin-left:395px">
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Contact Admin</a></li>
                    <li><a href="#">Others</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?=base_url()?>logout">Logout</a></li>
                  </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="dashboard-user" class="soft-bg" style="padding:30px 0;">
<div class="container">
    <div class="row">
        <div class="col-md-3">
            