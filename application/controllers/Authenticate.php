<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate extends CI_Controller
{

    public $message;

    function __construct()
    {
        parent::__construct();
        $this->load->model('setting_m', 'ST');
        $this->load->model('authenticate_m', 'AM');
        $this->load->model('user_m', 'UM');
    }

    public function index()
    {
        if ($this->session->has_userdata('datauser')) {
            $sess = $this->session->userdata('datauser');
            if ($sess['logged_in']) {
                $this->check_role();
            }
        }
        $data['segment'] = 'Login |';
        
        $this->load->view('header', $data);
        $this->load->view('general/login');
        
    }
    
    public function signup()
    {
        $sponsor = $this->input->post('user_sponsor', TRUE);
        // get role and id_sponsor
        $seller_code = $this->ST->get_setting_by_alias('code_register_seller');
        if($sponsor == $seller_code){
            $data['role'] = SELLER;
            $data['id_sponsor'] = 0;
        } else {
            $data['role'] = MEMBER;
            $data['id_sponsor'] = $this->ST->get_by_id_single_row('ki_m_user',$sponsor,'usertologin')->id_user;
        }
        $data['name'] = ucwords(strtolower($this->input->post('name', TRUE)));
        $data['address'] = $this->input->post('alamat', TRUE);
        $data['city'] = strtoupper($this->input->post('kota', TRUE));
        $data['handphone'] = $this->input->post('handphone', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['bank_account_number'] = $this->input->post('norekening', TRUE);
        $data['bank_name'] = strtoupper($this->input->post('nmbank', TRUE));
        $data['ppass'] = md5($this->input->post('password', TRUE));
        $data['ppin'] = md5($this->input->post('pin', TRUE));
        
        $data['date_created'] = date('Y-m-d H:i:s');
        $last_id = $this->ST->get_total_all_data('ki_m_user');
        $data['usertologin'] = 'KI'.str_pad($last_id+1,8,"0",STR_PAD_LEFT);
        
        $id_user = $this->UM->signup($data);
        
        $datauser = array(
            'id_user' => $id_user,
            'name' => $data['name'],
            'usertologin' => $data['usertologin'],
            'email' => $data['email'],
            'logged_in' => TRUE,
            'role' => $data['role'],
            'handphone' => $data['handphone'],
        );
        
        $message['id_user'] = $id_user;
        
        $this->session->set_userdata('datauser', $datauser);

        echo json_encode($message);
    }
    
    public function signup_admin()
    {
        $data['role'] = ADMIN;
        $data['id_sponsor'] = 0;
        $data['name'] = ucwords(strtolower($this->input->post('name', TRUE)));
        $data['address'] = $this->input->post('alamat', TRUE);
        $data['city'] = strtoupper($this->input->post('kota', TRUE));
        $data['handphone'] = $this->input->post('handphone', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['bank_account_number'] = $this->input->post('norekening', TRUE);
        $data['bank_name'] = strtoupper($this->input->post('nmbank', TRUE));
        $data['ppass'] = md5($this->input->post('password', TRUE));
        $data['ppin'] = md5($this->input->post('pin', TRUE));
        
        $data['date_created'] = date('Y-m-d H:i:s');
        $last_id = $this->ST->get_total_all_data('ki_m_user');
        $data['usertologin'] = 'ADM'.str_pad($last_id+1,8,"0",STR_PAD_LEFT);
        
        $id_user = $this->UM->signup($data);
        
        $datauser = array(
            'id_user' => $id_user,
            'name' => $data['name'],
            'usertologin' => $data['usertologin'],
            'email' => $data['email'],
            'logged_in' => TRUE,
            'role' => $data['role'],
            'handphone' => $data['handphone'],
        );
        
        $message['id_user'] = $id_user;
        
        $this->session->set_userdata('datauser', $datauser);

        echo json_encode($message);
    }

    public function login()
    {
        if ($_POST) {
            $e = $this->input->post("usertologin", TRUE);
            $p = $this->input->post("password", TRUE);
            //$p = md5($p);
            //$s = $this->input->post("step", TRUE);

            if ($this->authenticate($e, $p)) {
                $user = $this->AM->get_user($e, $p);
                $message['name'] = 'Hi, ' . $user->name;

                $datauser = array(
                    'id_user' => $user->id_user,
                    'usertologin' => $user->usertologin,
                    'name' => $user->name,
                    'email' => $user->email,
                    'logged_in' => TRUE, 
                    'role' => $user->role,
                    'handphone' => $user->handphone,
                    );
            
                $this->session->set_userdata('datauser', $datauser);
                $message['name'] = $user->name;
                $message['success'] = TRUE;
            } else {
                $message['success'] = FALSE;
                $message['false'] = 'User atau password yang Anda masukkan tidak cocok';
            }
        } else {
            $message['success'] = FALSE;
            $message['false'] = "Masukkan email atau password Anda";
        }
        echo json_encode($message);
    }
    
    public function authenticate($e, $p)
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('usertologin', 'usertologin', 'required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required|xss_clean');

        if ($this->form_validation->run()) {
            if ($this->AM->is_valid($e, $p)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    
    public function check_role()
    {
        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');
            $id_user = $sess['id_user'];

            $role = $this->AM->get_role($id_user);

            if ($role == 0 || $role == 1) {
                redirect('/dashboard');
            } else if ($role == 2) {
                redirect('/admin');
            } else {
                redirect('/');
            }

        } else {
            redirect('/');
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/');
    }
}