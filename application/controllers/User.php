<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    public $message;
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('setting_m', 'ST');
        $this->load->model('user_m', 'UM');
        $this->load->model('authenticate_m', 'AM');
        is_valid_member_seller();
    }
    
    public function dashboard_page()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Dashboard |';
        $data['is_new_member'] = $this->UM->is_new_member($id_user);
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/dashboard', $data);
        $this->load->view('footer');
    }
    
    public function register_saham()
    {   
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Beli Saham |';
        $data['harga_saham'] = $this->ST->get_setting_by_alias('harga_saham');
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/register_saham', $data);
        $this->load->view('footer');
    }
    
    public function do_saham()
    {
        $sess = $this->session->userdata('datauser');
        $point = $this->ST->get_setting_by_alias('point_saham');
        $jmlsaham = $this->input->post('jmlsaham', true);
        
        $data['saldo'] =  $jmlsaham * $point;
        $condition = [ 'id_user' =>  $sess['id_user'], ];
        
        $this->ST->update_array('ki_m_user',$data,$condition);
        
    }
    
    public function aktivasi_saham()
    {   
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Aktivasi Saham |';
        $data['point_saham'] = $this->ST->get_setting_by_alias('point_saham');
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $max_activate_saham = $this->ST->get_setting_by_alias('max_activate_saham');
        if($data['user']->inactive_lot > $max_activate_saham){
            $max_activate = $max_activate_saham;
        } else {
            $max_activate = $data['user']->inactive_lot;
        }
        $data['max_activate_saham'] = $max_activate;
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/aktivasi_saham', $data);
        $this->load->view('footer');
    }
    
    public function do_aktivasi_saham()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $point_amal = (float) $this->ST->get_setting_by_alias('point_amal');
        $point_administrasi = (float) $this->ST->get_setting_by_alias('point_administrasi');
        $point_dana_bersama = (float) $this->ST->get_setting_by_alias('point_dana_bersama');
        
        $jmlsaham = $this->input->post('jmlsaham', true);
        
        $data['active_lot'] = $user_data->active_lot + $jmlsaham;
        $data['inactive_lot'] = $user_data->inactive_lot - $jmlsaham;
        $condition = [ 'id_user' =>  $id_user, ];
        // update lot data in ki_m_user
        $this->ST->update_array('ki_m_user',$data,$condition);
        
        $point_kas = $this->ST->get_by_id_multiple_rows('ki_m_transaction_code',1,'id_transaction_group');
        foreach($point_kas as $point) {
            $data2['id_transaction_code'] = $point->id_transaction_code;
            $data2['id_user'] = $id_user;
            $data2['description'] = 'Aktivasi Saham - '.$point->description;
            if($point->id_transaction_code == 1){
                $data2['value'] = $point_dana_bersama * $jmlsaham;
            } else if($point->id_transaction_code == 2){
                $data2['value'] = $point_administrasi * $jmlsaham;
            } else if($point->id_transaction_code == 3) {
                $data2['value'] = $point_amal * $jmlsaham;
            }
            $data2['isdk'] = 0;
            $data2['date_transaction'] = date('Y-m-d H:i:s');
            //insert to transaction
            $this->ST->insert('ki_t_transaction_point',$data2);
        }      
    }
}
