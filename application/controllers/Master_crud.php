<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_Crud extends CI_Controller
{
    
    public $message;

    function __construct()
    {
        parent::__construct();
        $this->load->model('authenticate_m', 'AM');
        $this->load->model('admin_m', 'ADM');
        $this->load->model('setting_m', 'ST');
        $this->load->model('order_m', 'OM');
        $this->load->model('followup_m', 'FM');
        $this->load->model('crud_m', 'CM');
        $this->AM->valid_admin();
    }
    
    public function index()
    {
        
    }
    
    /** MASTER CAR TYPE **/
    public function car_type_all()
    {
        $data['name'] = NULL;
        $data['header'] = NULL;

        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');

            if ($sess['role'] != 2) {
                redirect('/');
            }

            $data['name'] = 'Halo, ' . $sess['name'] . '!';
            $data['menu'] = menu_user($sess['role']);
            $data['car'] = $this->CM->get_all_car();

            $this->load->view('header', $data);
            $this->load->view('content/frontend/general/menu', $data);
            $this->load->view('content/backend/master_crud/car_type_all', $data);
            $this->load->view('footer');
        } else {
            redirect('/');
        } 
    }
    
    public function car_type_create()
    {
        $data['name'] = NULL;
        $data['header'] = NULL;

        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');

            if ($sess['role'] != 2) {
                redirect('/');
            }

            $data['name'] = 'Halo, ' . $sess['name'] . '!';
            $data['menu'] = menu_user($sess['role']);
            $data['brand'] = $this->CM->get_all_brand();
            $id_type = $this->input->get('id_type');
            if(isset($id_type)) {
                $data['data'] = $this->CM->get_car_detail($id_type);
                $data['color'] = $this->ST->get_by_id_multiple_rows('master_car_color',$id_type,'id_type');
            }
            
            $this->load->view('header', $data);
            $this->load->view('content/frontend/general/menu', $data);
            $this->load->view('content/backend/master_crud/car_type_create', $data);
            $this->load->view('footer');
        } else {
            redirect('/');
        } 
    }
    
    public function ajax_model_by_brand()
    {
        $id_brand = $this->input->post('id_brand');
		
		$model = $this->CM->get_model_by_brand($id_brand);
		
		$result = $model->result();

		echo json_encode($result);
        return '';
    }
    
    public function car_type_insert()
    {   
         $now = date('Y-m-d H:i:s');
         $id_brand = $this->input->post('id_brand', TRUE);
         $id_model = $this->input->post('id_model', TRUE);
         //$id_type = $this->input->post('id_type', TRUE);
         $type = $this->input->post('type', TRUE);

         //get color
         $color = $this->input->post('color', TRUE);
         $red = $this->input->post('red', TRUE);
         $green = $this->input->post('green', TRUE);
         $blue = $this->input->post('blue', TRUE);
         $color_price = $this->input->post('color_price', TRUE);
         
         $code_model = $this->ST->get_by_id_single_row('master_car_model',$id_model,'id_model')->code_model;
         $last_code = $this->CM->get_type_last_code($id_model) + 1 ;
         $new_code_type = $code_model.'-'.$last_code;
         $data['code_type'] = $new_code_type;
         $data['id_model'] = $id_model;
         $data['type_url'] = str_replace('"','',str_replace('/','',str_replace(' ','-',strtolower($type))));

         $data['type'] = $type;
         $data['transmission'] = $this->input->post('transmission', TRUE);
         $data['fuel'] = $this->input->post('fuel', TRUE);
         $data['msrp'] = str_replace('.','',$this->input->post('price', TRUE));
         $data['exterior'] = $this->input->post('exterior', TRUE);
         $data['interior'] = $this->input->post('interior', TRUE);
         $data['info'] = $this->input->post('info', TRUE);
         $data['others'] = $this->input->post('others', TRUE);
         $data['is_show'] = '1';
         $data['is_show_desc'] = '1';
         $data['last_update'] = $now;
         //print_r($data); die();
         //insert to master_car_type
         $id_type = $this->CM->insert_car_type($data);

        if($color!=null){
        $total_color = $this->ST->get_total_row('master_car_color',$id_type,'id_type');
        
        foreach($color AS $key => $c){
            $total_color++;
            $code_color = $data['code_type'].'-'.$total_color;

            $input_data[$key] = [
                'code_color' => $code_color,
                'color' => $color[$key],
                'red' => $red[$key],
                'green' => $green[$key],
                'blue' => $blue[$key],
                'color_price' => replaceDot($color_price[$key]),
                'id_type' => $id_type,
                'last_update' => $now,
            ];
        }

        //insert tp master_car_color
        $this->ST->insert_batch('master_car_color',$input_data);
        } 
        redirect('master_crud/car_type_all');
    }
    
    public function car_type_edit()
    {
        $data['name'] = NULL;
        $data['header'] = NULL;

        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');

            if ($sess['role'] != 2) {
                redirect('/');
            }

            $data['name'] = 'Halo, ' . $sess['name'] . '!';
            $data['menu'] = menu_user($sess['role']);

            $id_type = $this->input->get('id_type');
            $data['car'] = $this->CM->get_car_detail($id_type);
            $data['brand'] = $this->CM->get_all_brand();
            $data['model'] = $this->CM->get_model_by_brand($data['car']->id_brand);
            $data['all_color'] = $this->ST->get_by_id_multiple_rows('master_car_color',$id_type,'id_type');
            
            $this->load->view('header', $data);
            $this->load->view('content/frontend/general/menu', $data);
            $this->load->view('content/backend/master_crud/car_type_edit', $data);
            $this->load->view('footer');
        } else {
            redirect('/');
        } 
    }
    
    public function car_type_update()
    {
         $now = date('Y-m-d H:i:s');
         $id_brand = $this->input->post('id_brand', TRUE);
         $id_model = $this->input->post('id_model', TRUE);
         $id_type = $this->input->post('id_type', TRUE);
         $type = $this->input->post('type', TRUE);
         $current_model = $this->ST->get_by_id_single_row('master_car_type',$id_type,'id_type')->id_model;
         $code_type = $this->ST->get_by_id_single_row('master_car_type',$id_type,'id_type')->code_type;
         //echo $current_model.'-'.$id_model; die();
         
         if($current_model!=$id_model) {
             $code_model = $this->ST->get_by_id_single_row('master_car_model',$id_model,'id_model')->code_model;
             $last_code = $this->CM->get_type_last_code($id_model) + 1 ;
             $code_type = $code_model.'-'.$last_code;
             $data['code_type'] = $code_type;
             $data['id_model'] = $id_model;
             //echo $current_model.'-'.$id_model.'-'.$code_type; die();
         }
         
         //get color
         $color = $this->input->post('color', TRUE);
         $red = $this->input->post('red', TRUE);
         $green = $this->input->post('green', TRUE);
         $blue = $this->input->post('blue', TRUE);
         $color_price = $this->input->post('color_price', TRUE);

         $data['type_url'] = str_replace('"','',str_replace('/','',str_replace(' ','-',strtolower($type))));
         $data['type'] = $type;
         $data['transmission'] = $this->input->post('transmission', TRUE);
         $data['fuel'] = $this->input->post('fuel', TRUE);
         $data['msrp'] = str_replace('.','',$this->input->post('price', TRUE));
         $data['exterior'] = $this->input->post('exterior', TRUE);
         $data['interior'] = $this->input->post('interior', TRUE);
         $data['info'] = $this->input->post('info', TRUE);
         $data['others'] = $this->input->post('others', TRUE);

         //update master_car_type
         $update = $this->CM->update_car_type($id_type,$data);

        //delete master_car_color
        $this->ST->delete_by_id('master_car_color',$id_type,'id_type');
        
        if ($color != null) {
            $total_color = $this->ST->get_total_row('master_car_color',$id_type,'id_type');
            
            foreach($color AS $key => $c){
                $total_color++;
                $code_color = $code_type.'-'.$total_color;
    
                $input_data[$key] = [
                    'code_color' => $code_color,
                    'color' => $color[$key],
                    'red' => $red[$key],
                    'green' => $green[$key],
                    'blue' => $blue[$key],
                    'color_price' => replaceDot($color_price[$key]),
                    'id_type' => $id_type,
                    'last_update' => $now,
                ];
            }
    
            //insert tp master_car_color
            $this->ST->insert_batch('master_car_color',$input_data);
        } 
         redirect('master_crud/car_type_all');
    }
    
    public function delete_car_type()
    {
        $id_type = $this->input->post('id_type', TRUE);
        $this->CM->delete_car_type($id_type);
        redirect('master_crud/car_type_all');
    }    

    public function car_type_mass_price_edit()
    {
        $data['name'] = NULL;
        $data['header'] = NULL;

        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');

            if ($sess['role'] != 2) {
                redirect('/');
            }

            $data['name'] = 'Halo, ' . $sess['name'] . '!';
            $data['menu'] = menu_user($sess['role']);

            $data['car'] = $this->CM->get_all_car();
            
            $this->load->view('header', $data);
            $this->load->view('content/frontend/general/menu', $data);
            $this->load->view('content/backend/master_crud/car_type_mass_price_edit', $data);
            $this->load->view('footer');
        } else {
            redirect('/');
        } 
    }

    public function car_type_mass_price_update()
    {
        $sess = $this->session->userdata('datauser');
        if ($sess['role'] != 2) {
            redirect('/');
        }

        $id_type = replaceDot($this->input->post('id_type', TRUE));
        $msrp = replaceDot($this->input->post('msrp', TRUE));

        foreach($id_type AS $key => $type){
            if($msrp[$key] != '' && $msrp[$key] != 0){
                $save[] = [
                    'id_type'=>$id_type[$key],
                    'msrp'=>$msrp[$key]
                ];
            }
        }

        if(!empty($save)){
            $this->ST->update_batch('master_car_type',$save,'id_type');
        }
         
        redirect('master_crud/car_type_mass_price_edit');
    }
    
    public function car_type_all_deleted()
    {
        $data['name'] = NULL;
        $data['header'] = NULL;

        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');

            if ($sess['role'] != 2) {
                redirect('/');
            }

            $data['name'] = 'Halo, ' . $sess['name'] . '!';
            $data['menu'] = menu_user($sess['role']);
            $data['car'] = $this->CM->get_all_car_deleted();

            $this->load->view('header', $data);
            $this->load->view('content/frontend/general/menu', $data);
            $this->load->view('content/backend/master_crud/car_type_all_deleted', $data);
            $this->load->view('footer');
        } else {
            redirect('/');
        } 
    }
    
    public function restore_car_type()
    {
        $id_type = $this->input->post('id_type', TRUE);
        $this->CM->restore_car_type($id_type);
        redirect('master_crud/car_type_all');
    } 
    /** END MASTER CAR TYPE **/

    /** MASTER CAR BRAND **/
    public function car_brand_all()
    {
        $data['name'] = NULL;
        $data['header'] = NULL;

        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');

            if ($sess['role'] != 2) {
                redirect('/');
            }

            $data['name'] = 'Halo, ' . $sess['name'] . '!';
            $data['menu'] = menu_user($sess['role']);

            $data['all_data'] = $this->ST->get_by_id_multiple_rows('master_car_brand',1,'is_show');

            $this->load->view('header', $data);
            $this->load->view('content/frontend/general/menu', $data);
            $this->load->view('content/backend/master_crud/car_brand_all', $data);
            $this->load->view('footer');
        } else {
            redirect('/');
        } 
    }

    public function car_brand_create()
    {
        $data['name'] = NULL;
        $data['header'] = NULL;

        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');

            if ($sess['role'] != 2) {
                redirect('/');
            }

            $data['name'] = 'Halo, ' . $sess['name'] . '!';
            $data['menu'] = menu_user($sess['role']);

            $data['brand'] = $this->CM->get_all_brand();
            
            $this->load->view('header', $data);
            $this->load->view('content/frontend/general/menu', $data);
            $this->load->view('content/backend/master_crud/car_brand_create', $data);
            $this->load->view('footer');
        } else {
            redirect('/');
        } 
    }

    public function car_brand_insert()
    {
        $brand = $this->input->post('brand', TRUE);
        $logo_brand = $this->input->post('logo_brand');
        
        $last_id_brand = $this->ST->get_total_all_data('master_car_brand') + 1;
        /*if (strlen($last_id_brand) == 1 ) {
            $last_id_brand = '0'.$last_id_brand;
        }*/
        $last_id_brand = 'M-'.$last_id_brand;

        $logo_brand_name = '';

        //upload logo brand
        if(!empty($_FILES['logo_brand']['name']))
        {
            $config['upload_path'] = PATH_IMAGE_LOGO_CAR;
            $config['allowed_types'] = '*';
            $config['file_name'] = strtolower($brand);
            $config['max_size']  = '0'; //0 = nolimit
            $config['max_width']  = '0'; //0 = nolimit
            $config['max_height']  = '0'; //0 = nolimit
            $config['overwrite'] = true;
            
            $this->load->library('upload', $config); #include library
            $upload = $this->upload->do_upload('logo_brand');
            
            if($upload == FALSE) //ini validasi kosong
            {
                $msg_1 = $this->upload->display_errors();
            }   
            else
            {
                $msg_1 = $this->upload->data();
            }

            //get the logo brand with extension name
            $logo_brand_name = $config['file_name'].$msg_1['file_ext'];
        }

        $data = [
            'code_brand' => $last_id_brand,
            'brand' => $brand,
            'logo_brand' => $logo_brand_name,
            'is_show' => 1,
        ];

        //insert to master_car_brand
        $this->ST->insert('master_car_brand',$data);
         
        redirect('master_crud/car_brand_all');
    }

    public function car_brand_edit($id)
    {
        $data['name'] = NULL;
        $data['header'] = NULL;

        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');

            if ($sess['role'] != 2) {
                redirect('/');
            }

            $data['name'] = 'Halo, ' . $sess['name'] . '!';
            $data['menu'] = menu_user($sess['role']);

            $data['path'] = base_url().URL_PATH_IMAGE_LOGO_CAR;
            $data['data'] = $this->ST->get_by_id_single_row('master_car_brand',$id,'id_brand');
            
            $this->load->view('header', $data);
            $this->load->view('content/frontend/general/menu', $data);
            $this->load->view('content/backend/master_crud/car_brand_edit', $data);
            $this->load->view('footer');
        } else {
            redirect('/');
        } 
    }

    public function car_brand_update()
    {
        $data['name'] = NULL;
        $data['header'] = NULL;

        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');

            if ($sess['role'] != 2) {
                redirect('/');
            }

            $id = $this->input->post('id_brand');
            $brand = $this->input->post('brand');
            $logo_brand = $this->input->post('logo_brand');

            $logo_brand_name = '';

            //upload logo brand
            if(!empty($_FILES['logo_brand']['name']))
            {
                //remove image
                $condition_1 = [
                    'id_brand' => $id
                ];
                $get_data = $this->ST->get_single_data_by_array('master_car_brand',$condition_1);
                if($get_data->logo_brand != ''){
                    unlink(PATH_IMAGE_LOGO_CAR.$get_data->logo_brand);
                }

                $config['upload_path'] = PATH_IMAGE_LOGO_CAR;
                $config['allowed_types'] = '*';
                $config['file_name'] = strtolower($brand);
                $config['max_size']  = '0'; //0 = nolimit
                $config['max_width']  = '0'; //0 = nolimit
                $config['max_height']  = '0'; //0 = nolimit
                $config['overwrite'] = true;
                
                $this->load->library('upload', $config); #include library
                $upload = $this->upload->do_upload('logo_brand');
                
                if($upload == FALSE) //ini validasi kosong
                {
                    die('aa');
                    $msg_1 = $this->upload->display_errors();
                }   
                else
                {
                    $msg_1 = $this->upload->data();
                }

                //get the logo brand with extension name
                $logo_brand_name = $config['file_name'].$msg_1['file_ext'];

                $condition = [
                    'id_brand' => $id
                ];
                $data = [
                    'brand' => $brand,
                    'logo_brand' => $logo_brand_name,
                ];
                //update master_car_brand
                $this->ST->update_array('master_car_brand',$data,$condition);
            }else{
                $condition = [
                    'id_brand' => $id
                ];
                $data = [
                    'brand' => $brand,
                ];
                //update master_car_brand
                $this->ST->update_array('master_car_brand',$data,$condition);
            }
             
            redirect('master_crud/car_brand_all');
        } else {
            redirect('/');
        } 
    }

    public function delete_car_brand()
    {
        $id = $this->input->post('id_brand', TRUE);
        $condition = [
            'id_brand' => $id
        ];
        $data = [
            'is_show' => 0,
        ];
        //update master_car_brand set is_show 0
        $this->ST->update_array('master_car_brand',$data,$condition);
        redirect('master_crud/car_brand_all');
    }
    
    public function car_brand_all_deleted()
    {
        $data['name'] = NULL;
        $data['header'] = NULL;

        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');

            if ($sess['role'] != 2) {
                redirect('/');
            }

            $data['name'] = 'Halo, ' . $sess['name'] . '!';
            $data['menu'] = menu_user($sess['role']);
            $data['all_data'] = $this->ST->get_by_id_multiple_rows('master_car_brand','0','is_show');

            $this->load->view('header', $data);
            $this->load->view('content/frontend/general/menu', $data);
            $this->load->view('content/backend/master_crud/car_brand_all_deleted', $data);
            $this->load->view('footer');
        } else {
            redirect('/');
        } 
    }
    
    public function restore_car_brand()
    {
        $id = $this->input->post('id_brand', TRUE);
        $condition = [
            'id_brand' => $id
        ];
        $data = [
            'is_show' => 1,
        ];
        //update master_car_brand set is_show 0
        $this->ST->update_array('master_car_brand',$data,$condition);
        redirect('master_crud/car_brand_all');
    }
    /** END MASTER CAR BRAND **/

}   