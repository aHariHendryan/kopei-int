<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public $message;

    function __construct()
    {
        parent::__construct();
        $this->load->model('setting_m', 'ST');
        $this->load->model('user_m', 'UM');
        $this->load->model('admin_m', 'AM');
    }

    public function index()
    {
        
    }
    
    public function login()
    {
        if ($_POST) {
            $e = $this->input->post("email", TRUE);
            $p = $this->input->post("password", TRUE);
            //$p = md5($p);
            $s = $this->input->post("step", TRUE);

            if ($this->authenticate($e, $p)) {
                $user = $this->AM->get_user($e, $p);
                $message['name'] = 'Halo, ' . $user->fullname . '!';

                $flag = TRUE;
                if ($this->AM->is_dealer($user->email)) {
                    if ($s == "true") {
                        $message['false'] = 'Dealer tidak dapat melakukan order';
                        $flag = FALSE;
                    } else {
                        $datauser = array(
                            'id_user' => $user->id_user,
                            'name' => $user->fullname,
                            'email' => $user->email,
                            'city' => $user->city,
                            'phone' => $user->handphone,
                            'brand' => $user->brand,
                            'logged_in' => TRUE,
                            'role' => $user->role,
                            'id_dealer' => $user->id_dealer,
                            'remember_token' => '',
                        );
                    }
                } else {
                    $datauser = array(
                        'id_user' => $user->id_user,
                        'name' => $user->fullname,
                        'email' => $user->email,
                        'logged_in' => TRUE,
                        'phone' => $user->handphone,
                        'role' => $user->role,
                        'remember_token' => '',
                    );
                }

                if ($flag) {
                    $this->session->set_userdata('datauser', $datauser);

                    //update remember token
                    update_remember_token();

                    $message['email'] = $user->email;
                    $message['menu'] = menu_user($user->role);
                    $message['number_of_order'] = $this->SM->get_number_of_active_order($user->id_user);
                    $message['limit_max_order'] = (int)$this->ST->get_setting_by_alias('limit_max_order_unique_email');
                }
            } else {
                $message['false'] = 'Email atau password yang Anda masukkan tidak cocok';
            }
        } else {
            $message['false'] = "Masukkan email atau password Anda";
        }

        echo json_encode($message);
    }

    public function check_role()
    {
        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');
            $email = $sess['email'];

            $role = $this->AM->get_role($email);

            if ($role == 0) {
                redirect('/user/my_order');
            } else if ($role == 1) {
                redirect('/dealer/all_orders_latest');
            } else if ($role == 2) {
                redirect('/admin/admin_control/');
            } else {
                redirect('/');
            }

        } else {
            redirect('/');
        }
    }

    public function authenticate($e, $p)
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'Email', 'required|xss_clean|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|xss_clean');

        if ($this->form_validation->run()) {
            if ($this->AM->is_valid($e, $p)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/');
    }

    public function my_account()
    {
        //check if have no phone number
        check_has_phone_number();

        $data['header'] = NULL;
        $this->AM->valid_login();
        
        $sess = $this->session->userdata('datauser');
        $user = $this->ST->get_by_id_single_row('ki_m_user',$sess['id_user'],'id_user');
        $data['name'] = $user->fullname;
        $data['email'] = $user->email;
        $data['handphone'] = $user->handphone;
    
        $this->load->view('header', $data);
        $this->load->view('content/backend/general/edit_profile');
        $this->load->view('footer');
    }


    public function set_account()
    {
        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');
            $lastemail = $sess['email'];
            $lastname = $sess['name'];
            $lastlogged_in = $sess['logged_in'];
            $lastrole = $sess['role'];
            $lastphone = $sess['phone'];
            if($sess['role'] == 1)
                $lastcity = $sess['city'];
            else
                $lastcity = null;
            $last_iduser = $sess['id_user'];
            if($lastrole == 1)
                $lastbrand = $sess['brand'];
            else
                $lastbrand = '';

            $name = $this->input->post('nameacc', TRUE);
            $phone = $this->input->post('phoneacc', TRUE);
            $email = $this->input->post('emailacc', TRUE);
            $pass = $this->input->post('passacc', TRUE);
            if (!IsNullOrEmptyString($pass)) {
                $pass = md5($pass);
            }


            $message = 'Data';

            if (!IsNullOrEmptyString($name)) {
                $this->AM->set_account('fullname', $name, $lastemail);
                $message .= ' nama, ';
            }

            if (!IsNullOrEmptyString($phone)) {
                $this->AM->set_account('handphone', $phone, $lastemail);
                $message .= ' handphone, ';
            }

            if (!IsNullOrEmptyString($pass)) {
                $this->AM->set_account('password', $pass, $lastemail);
                $message .= ' password, ';
            }

            if (!IsNullOrEmptyString($email)) {
                $this->AM->set_account('email', $email, $lastemail);
                $this->AM->set_account_dealer('email', $email, $lastemail);
                $message .= ' email, ';  
            }

            $datauser = array(
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'logged_in' => $lastlogged_in,
                'role' => $lastrole,
                'city' => $lastcity,
                'brand' => $lastbrand,
                'id_user' => $last_iduser,
            );
            $this->session->set_userdata('datauser', $datauser);

            $message .= 'berhasil diubah';

            redirect('/my_account');
        } else {
            redirect('/');
        }
    }

    public function update_profile()
    {
        //validate login
        check_login();

        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');

            //$data['handphone'] = $this->input->post('handphone', TRUE);
            $data['email'] = $this->input->post('email', TRUE);
            $password = $this->input->post('passacc', TRUE);
            $conf_password = $this->input->post('conf_passacc', TRUE);
            
            if (!IsNullOrEmptyString($password) && $password == $conf_password) {
                $data['password'] = md5($password);
            }

            $condition = [
                'id_user' => $sess['id_user'],
            ];

            //print_r($data);die();

            //update data profile
            $this->ST->update_array('ki_m_user',$data,$condition);

            update_remember_token();

            $message = 'Profile anda sukses diupdate';

            redirect('/mulmut/my_account');
        } else {
            redirect('/');
        }
    }

public function register_admin()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Register Admin |';
        $data['is_new_member'] = $this->UM->is_new_member($id_user);
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/register_admin');
        $this->load->view('footer');
    }
    

public function dashboard_page()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Dashboard |';
        $data['is_new_member'] = $this->UM->is_new_member($id_user);
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/dashboard', $data);
        $this->load->view('footer');
    }


public function keuntungan_harian()
    {   
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Keuntungan Harian |';
        $data['max_nominal'] = $this->ST->get_setting_by_alias('max_daily_point');
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/point_harian', $data);
        $this->load->view('footer');
    }

public function do_bhh() {
    $inputvalue = $this->input->post('pointharian', true);
    $this->AM->doupdate_user_without_transaction($inputvalue);
}

}
