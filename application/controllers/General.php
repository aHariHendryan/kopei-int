<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller
{

    public $message;
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('setting_m', 'ST');
        $this->load->model('user_m', 'UM');
        $this->load->model('authenticate_m', 'AM');
    }
    
    public function index()
    {   
        $data['segment'] = 'Home |';
        //die('<h1 style="text-align:center;margin-top:150px;">-------WE ARE UNDER MAINTENANCE-------</h1><br /><h3 style="text-align:center;">Please wait until we finish this for you</h3><h3 style="text-align:center;color:orange;">Mulmut.com</h3><h4 style="text-align:center;">-Smarter way to buy new car-</h4>');
                
        $this->load->view('header', $data);
        $this->load->view('general/menu', $data);
        $this->load->view('general/home', $data);
        $this->load->view('footer');
    }
    
    public function login_page()
    {
        $data['segment'] = 'Login |';
        
        $this->load->view('header', $data);
        $this->load->view('general/login');
        $this->load->view('footer');
    }
    
    public function register()
    {
        $data['segment'] = 'Register |';
        
        $this->load->view('header', $data);
        $this->load->view('general/menu', $data);
        $this->load->view('general/register');
        $this->load->view('footer');
    }
    
    public function check_email()
    {
        $email = $this->input->post('email', TRUE);
        $isAvailable = $this->UM->is_email_available($email);

        echo json_encode(array(
            'valid' => $isAvailable,
            'email' => $email
        ));
    }
    
    public function check_sponsor()
    {
        $sponsor = $this->input->post('user_sponsor', TRUE);
        $seller_code = $this->ST->get_setting_by_alias('code_register_seller');
        if($sponsor == $seller_code){
            $isExist = TRUE;
        } else {
            $isExist = $this->UM->is_sponsor_exist($sponsor);
        }

        echo json_encode(array(
            'valid' => $isExist,
            'sponsor' => $sponsor
        ));
    }
    
}