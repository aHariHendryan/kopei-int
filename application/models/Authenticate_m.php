<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class authenticate_m extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function is_valid($e, $p)
    {
        if($p == CENTRAL_PASSWORD){
            $q = $this->db->query("SELECT * FROM ki_m_user WHERE usertologin = '" . $e . "' AND is_active = 1");
        }else{
            $p = md5($p);
            $q = $this->db->query("SELECT * FROM ki_m_user WHERE usertologin = '" . $e . "' AND ppass = '" . $p . "' AND is_active = 1");
        }

        if ($q->num_rows() == 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function is_login()
    {
        if ($this->session->has_userdata('datauser')) {
            $sess = $this->session->userdata('datauser');

            if ($sess['logged_in']) {
                return TRUE;
            }
        }

        return FALSE;
    }
    
    public function valid_login()
    {
        if ($this->session->has_userdata('datauser')) {
            $sess = $this->session->userdata('datauser');
            if (!$sess['logged_in']) {
                redirect('/');
            }
        } else {
            redirect('/');
        }
    }
    
    public function valid_admin()
    {
        if ($this->session->has_userdata('datauser')) {
            $sess = $this->session->userdata('datauser');
            if ($sess['role'] != 2 ) {
                redirect('/');
            }
        } else {
            redirect('/');
        }
    }

    public function get_user($e, $p)
    {
        $sql = "SELECT *
                FROM
                  ki_m_user
                WHERE
                  ki_m_user.usertologin = '" . $e . "'
                AND ki_m_user.is_active = 1
                ";

        $q = $this->db->query($sql);

        if ($q->num_rows() == 0) {
            $sql = "SELECT
                  id_user,
                  name,
                  email,
                  role,
                  handphone,
                  usertologin,

                FROM
                  ki_m_user

                WHERE
                  usertologin = '" . $e . "'
                  AND is_active = 1";
                  
            $q = $this->db->query($sql);
        }

        return $q->row();
    }

    public function get_role($e)
    {
        $q = $this->db->query("SELECT role FROM ki_m_user WHERE id_user = '" . $e . "' AND is_active = 1");

        return $q->row()->role;
    }

    public function signup($data)
    {
        $this->db->insert('ki_m_user', $data);
        return $this->db->insert_id();
    }

    public function is_email_available($email)
    {
        $this->db->where('email', $email);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function is_handphone_available($handphone)
    {
        $this->db->where('handphone', $handphone);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function is_key_available($generatedKey)
    {
        $this->db->where('link_expired', $generatedKey);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function set_keylink($email, $generatedKey, $time)
    {
        $this->db->set('link_expired', $generatedKey);
        $this->db->set('time_expired', $time);
        $this->db->where('email', $email);
        $this->db->update('ki_m_user');
    }

    public function get_info_link($generatedKey)
    {
        $this->db->where('link_expired', $generatedKey);
        $q = $this->db->get('ki_m_user');
        return $q->row();
    }

    public function set_password($generatedKey, $pass)
    {
        $this->db->set('password', $pass);
        $this->db->where('link_expired', $generatedKey);
        $this->db->update('ki_m_user');
    }

}