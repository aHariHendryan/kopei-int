<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_m extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    public function is_new_member($id_user)
    {
        $this->db->where('DATEDIFF(now(), date_created) >= 2 and id_user = '.$id_user);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function is_email_available($email)
    {
        $this->db->where('email', $email);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function is_sponsor_exist($usertologin)
    {
        $this->db->where('usertologin', $usertologin);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return false;
        } else {
            return true;
        }
    }
    
    public function signup($data)
    {
        $this->db->insert('ki_m_user', $data);
        return $this->db->insert_id();
    }

    public function get_user_without_transaction() {
        $query = "SELECT * FROM `ki_m_user` WHERE `id_user` IN (SELECT id_user FROM `ki_t_transaction` WHERE   DATE(`date_transaction`)<>CURDATE())";

        $result = $this->db->query($query);
        return $result->result();
    }
    
    public function doupdate_user_without_transaction($var) {
        $d = $this->get_user_without_transaction();
        foreach ($d as $key => $value) {
            $lot = $value->active_lot;
            $data['saldo_point'] = ($var*$lot)/10;
            $this->db->where('id_user', $value->id_user);
            $this->db->update('ki_m_user', $data);
        }
        return true;
    }

}
