//thousand give dot when outfocus
$('.dotdigit').keyup(function(event) {
    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;

    // format number
    $(this).val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
      ;
    });
});

function pad(num, size) {
    return ('000000000' + num).substr(-size);
}

function rupiah(nStr) {
   nStr += '';
   x = nStr.split('.');
   x1 = x[0];
   x2 = x.length > 1 ? '.' + x[1] : '';
   var rgx = /(\d+)(\d{3})/;
   while (rgx.test(x1))
   {
      x1 = x1.replace(rgx, '$1' + '.' + '$2');
   }
   return "Rp " + x1 + x2;
}

var window_width = $(window).width();
var window_height = $(window).height();
var keys = {
    37: 1,
    38: 1,
    39: 1,
    40: 1
};

function preventDefault(e) {
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}


function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}


function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove = preventDefault; // mobile
    document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}


$(document).ready(function() {
    /***************** Nosrcoll Content when Menu active ******************/
    $("#st-trigger-effects > a").click(function() {
        disableScroll();
    });

    /***************** slimscroll menu ******************/
    /*$(function() {
        // var whight = $( window ).height();
        var h = window.innerHeight;
        var hside = h;
        $('.slimside').slimScroll({
            height: hside + 'px'
        });
    });*/

    /***************** Close Menu ******************/
    $(".sidebarlink a, .closemenu").click(function() {
        $('#st-container').removeClass('st-menu-open');
        enableScroll();
    });

    /***************** why choose fancybox ******************/
    $('#list-choosen div').click(function() {
        $('#list-choosen').slideUp();
    });

    $("#goMenu").click(function() {
        $(".wrap-childmenu").css("display","block");
        $("#closeMenu").show();
        $("#goMenu").hide();
    });

    $(document).mouseup(function(e) {
        var container1 = $(".wrap-childmenu");
        if (!container1.is(e.target) // if the target of the click isn't the container...
            && container1.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container1.hide();
            $("#goMenu").show();
            $("#closeMenu").hide();
        }
    });

    $("#closeMenu").click(function(){
        $(".wrap-childmenu").css("display","none");
        $("#goMenu").show();
        $("#closeMenu").hide();
    });

    $("#goRegister").click(function() {
        $(".wrap-login").hide();
        $(".wrap-reset").hide();
        $(".wrap-register").hide().fadeIn();
    });

    $("#goLogin").click(function() {
        $(".wrap-register").hide();
        $(".wrap-reset").hide();
        $(".wrap-login").hide().fadeIn();
    });

    $("#goReset").click(function () {
        $(".wrap-login").hide();
        $(".wrap-register").hide();
        $(".wrap-reset").hide().fadeIn();
    });
});


/******* New Order Start**********/

$(window).load(function() {
    // hide spinner order step two
    $(".loading").html('<div class="loading"></div>');
    $('.detail-warna-step1').show();
    $('.detail-warna-step2').show();
    
    $("#wrap-merek-model-city").load("mulmut/brand_step", function(){
        $(".loading").html('<div class="loading"></div>');
    }).hide().show();
    
    //$("#wrap-detail-warna").load( function(){
      //  $(".loading").html('<div class="loading"></div>');
    //}).hide().show();
    
    /***************** datepicker ******************/
    /*$('#showdatedesk.input-group.date').datepicker({
        todayHighlight: true,
        startDate: "today",
        autoclose: true,
        clearBtn: true,
        format: 'dd/mm/yyyy',
    });*/
    
    /***************** Choose Brand *************************/
    $("body").on("change", ".brand-group .next-step", function(e) {
        
        // Hide page
        $(".merek-model-step1").css("display", "none");
        $(".brand-group input[type='radio']").removeClass("next-step");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');
        
        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.brand = $(this).attr("brand");
        step.brand_id = this.value;
        //console.log(hash_token);
        
        // Show next page
        $("#wrap-merek-model-city").load("mulmut/model_step", {
            merek: step.brand_id,
            csrf_test_name: hash_token,
        }, function(){
            $(".loading").html('<div class="loading"></div>');
        }).hide().show();

    });
    
    /***************** Choose Model *************************/
    $("body").on("change", ".model-group .next-step", function(e) {

        // Hide page
        $(".merek-model-step2").css("display", "none");
        $(".model-group input[type='radio']").removeClass("next-step");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.model_id = this.value;
        step.model = $(this).attr("model");
        step.model_url = $(this).attr("model_url");
        
        // Show next page
        $("#wrap-merek-model-city").load("mulmut/city_step", {
            model: step.model_id,
            merek: step.brand_id,
            csrf_test_name: hash_token,
        }, function(){
            $(".loading").html('<div class="loading"></div>');
        }).hide().show();
        
    });
    
    /***************** Choose City *************************/
    $("body").on("change", ".city-group .next-step", function(e) {
        //alert('function baru');
        // Hide page
        $(".merek-model-step4").css("display", "none");
        $(".model-group input[type='radio']").removeClass("next-step");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.city_id = this.value;
        step.city = $(this).attr("city");
        //step.model = step.model.replace(/ /g, "-");
        step.model_url = step.model_url;
        // Show next page
        window.location='order/car-type?model='+step.model_url+'&city='+step.city+'&id_model='+step.model_id;
        
    });
    
    /***************** Back Button Model *************************/
    $("body").on("click", "#back-button-model", function(e) {
        
        // Hide page
        $(".merek-model-step2").css("display", "none");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Show next page
        $("#wrap-merek-model-city").load("mulmut/brand_step", function(){
            $(".loading").html('<div class="loading"></div>');
        }).hide().show();
    });
    
    /***************** Back Button City *************************/
    $("body").on("click", "#back-button-city", function(e) {
        
        // Hide page
        $(".merek-model-step4").css("display", "none");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Show next page
        $("#wrap-merek-model-city").load("mulmut/model_step", {
            merek: step.brand_id,
            csrf_test_name: hash_token,
        }, function(){
            // Hide spinner
            $(".loading").html('<div class="loading"></div>');

            // Update bubbles
            $("#wizard").steps("previous");
            $(".wizard > .steps ul>li.current").nextAll().removeClass("done");
        }).hide().show();
    });
    
/***************** Choose Type *************************/

    $("body").on("click", "#tabel-detail tr", function(e) {
        // Hide page
        $(".detail-warna-step1").css("display", "none");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.type_id = $(this).children().first().html();
        step.type = $(this).find(".type-td").html();
        step.msrp = $(this).find(".type-td").attr('msrp');
        step.city = $(this).find(".type-td").attr('city');
        step.transmission = $(this).find(".type-td").attr('transmission');
        step.fuel = $(this).find(".type-td").attr('fuel');
        step.type = step.type.replace(/ /g, " ");
        step.type_url = $(this).find(".type-td").attr('type_url');
        
        // Show next page
        window.location='car-color?type='+step.type_url+'&transmission='+step.transmission+'&fuel='+step.fuel+'&city='+step.city+'&id_type='+step.type_id;
        
    });   
    
    /***************** Choose Color *************************/

    //$("body").on("click", "#table-warna-choosen tr.color-tr", function(e) {
    //$(".color-choosen").on("click", function(e) {
    $(".btn-next-color").on("click", function(e) {
        // Hide have account 
        $('.have-account').hide();
        
        // Hide page
        $(".detail-warna-step2").css("display", "none");
        
        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        /*step.color_id = $(this).children().first().html();
        step.color = $(this).find(".color-td").html();
        step.color_price = $(this).find(".color-price").attr('color-price');*/
        
        var color = $(".color-multiple").map(function(i,v){return this.value;}).get().join(", ");
        var color_price = $(".color-price-multiple").map(function(i,v){return this.value;}).get().join(", ");
        step.color = color;
        step.color_price = color_price;
            
        step.color_id = $(this).attr('data-color-id');
        //step.color = $(this).attr('data-color-name');
        //step.color_price = $(this).attr('data-color-price');
        
        
        //$("#wrap-konfirmasi").css("display", "show");
        $("#wrap-konfirmasi").show();
        
        if ($("#is_login").val() == "true") {
            step.email_buyer = $("#email_buyer").val();
            //step.brand_id = $("#brand").val();
            step.transmission = $("#transmission").val();
            step.fuel = $("#fuel").val();
            step.type_id = $("#type_id").val();
            step.type = $("#type").val();
            step.city = $("#city").val();
            step.msrp = $("#msrp").val();
            step.model = $("#model").val();
            step.brand = $("#brand").val();
            step.number_of_order = $("#number_of_order").val();
            step.limit_max_order = $("#limit_max_order").val();
            $(".konfirmasi-step1").css("display", "none");
            
            if(parseInt(step.number_of_order) >= parseInt(step.limit_max_order)) {
                $("#too-many-order-warning").hide();
                $("#wrap-konfirmasi").load("error_step", function(){
                $(".loading").html('<div class="loading"></div>');
                }).hide().show();       
            } else {
                //$("#wrap-konfirmasi").load("finance_step", {
                $("#wrap-konfirmasi").load("last_step", {    
                    //store data
                    model: step.model_id,
                    merek: step.brand_id,
                    transmission: step.transmission,
                    fuel: step.fuel,
                    id_type: step.type_id,
                    type: step.type,
                    color: step.color_id,
                    color_name: step.color,
                    city: step.city,
                    msrp: step.msrp,
                    nominaldp: step.nominaldp,
                    color_price: step.color_price,
                    csrf_test_name: hash_token,
                }, function() {
                    // Get Phone Number & Fill in phone number field
                    $.ajax({
                        url: '../mulmut/get_phone_number',
                        type: 'POST',
                        data: {
                            email: step.email_buyer,
                            csrf_test_name: hash_token,
                        },
                        success: function(data) {
                            var obj = jQuery.parseJSON(data);
                            $('#handphone-field').val(obj.handphone);
                        }
                    });
    
                    // Hide spinner
                    $(".loading").html('<div class="loading"></div>');
    
                    // Update bubbles
                    //$("#wizard").steps("next");
    
                }).hide().show(); 
            } 
        } else {
            //alert('if false')
            // Show spinner
            //$(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');
            
            // Update bubbles
            //$("#wizard").steps("next");
            $("#wrap-login-confirm").show();
            $(".loading").html('<div class="loading"></div>');
                        
        }
        
        // Show next page
        /*$("#kota-step-paragraph1").html(step.brand + " " + step.type + " (" + step.transmission +
            ", " + step.fuel + "), " + step.color);
        if (step.color_price == 0 || step.color_price.length == 0){
            $("#kota-step-paragraph2").html(rupiah(step.msrp));
        } else {
            $("#kota-step-paragraph2").html(rupiah(step.msrp) + " + " + rupiah(step.color_price));
        }*/
        //$(".loading").html('<div class="loading"></div>');
        
        //$(".konfirmasi-step1").show();
        // Update bubbles
        //$("#wizard").steps("next");
        //$(".tipe-city-step").css("display", "block");

        // Hide spinner
        
    }); 
});


/******* New Order End **********/

/***************** Flexsliders ******************/
/*$(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: false
    });

    $('#portfolioSlider').flexslider({
        animation: "slide",
        directionNav: false,
        controlNav: true,
        touch: false,
        pauseOnHover: true,
        start: function() {
            $.waypoints('refresh');
        }
    });

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
            },
            600: {
                items: 1,
                nav: false,
            },
            1000: {
                items: 3,
                nav: true,
            }
        }
    })
}); */


/***************** fancybox ******************/

/*$(document).ready(function() {
    $(".fancybox").fancybox({
        arrows: true,
        padding: 0,
        closeBtn: true,
        openEffect: 'none',
        closeEffect: 'none',
        prevEffect: 'none',
        nextEffect: 'none',
        helpers: {
            media: {},
            overlay: {
                locked: false
            },
            buttons: false,
            thumbs: false,
            title: {
                type: 'inside'
            }
        },
        afterLoad: function() {
            this.title = '<h1 class="title-cap">' + this.title + '</h1>' + $(this.element).attr('alt');
        }
    });
});*/

/***************** Steps ******************/
/**$(function() {
    $("#wizard").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "none",
        enableKeyNavigation: false,
        enableAllSteps: false,
        enablePagination: false,
        onStepChanged: function(event, currentIndex, priorIndex) {
            $(".wizard > .steps ul>li.current").nextAll().addClass("disabled").attr("aria-disabled", "true").removeAttr("aria-selected");
        }
    });
});**/