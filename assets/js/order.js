//thousand give dot when outfocus
$('.dotdigit').keyup(function(event) {
    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;

    // format number
    $(this).val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
      ;
    });
});

function pad(num, size) {
    return ('000000000' + num).substr(-size);
}

function rupiah(nStr) {
   nStr += '';
   x = nStr.split('.');
   x1 = x[0];
   x2 = x.length > 1 ? '.' + x[1] : '';
   var rgx = /(\d+)(\d{3})/;
   while (rgx.test(x1))
   {
      x1 = x1.replace(rgx, '$1' + '.' + '$2');
   }
   return "Rp " + x1 + x2;
}

var window_width = $(window).width();
var window_height = $(window).height();
var keys = {
    37: 1,
    38: 1,
    39: 1,
    40: 1
};

function preventDefault(e) {
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}


function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}


function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove = preventDefault; // mobile
    document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}


$(document).ready(function() {
    /***************** Nosrcoll Content when Menu active ******************/
    $("#st-trigger-effects > a").click(function() {
        disableScroll();
    });

    /***************** slimscroll menu ******************/
    $(function() {
        // var whight = $( window ).height();
        var h = window.innerHeight;
        var hside = h;
        $('.slimside').slimScroll({
            height: hside + 'px'
        });
    });

    /***************** Close Menu ******************/
    $(".sidebarlink a, .closemenu").click(function() {
        $('#st-container').removeClass('st-menu-open');
        enableScroll();
    });

    /***************** why choose fancybox ******************/
    $('#list-choosen div').click(function() {
        $('#list-choosen').slideUp();
    });

    $("#goMenu").click(function() {
        $(".wrap-childmenu").css("display","block");
        $("#closeMenu").show();
        $("#goMenu").hide();
    });

    $(document).mouseup(function(e) {
        var container1 = $(".wrap-childmenu");
        if (!container1.is(e.target) // if the target of the click isn't the container...
            && container1.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container1.hide();
            $("#goMenu").show();
            $("#closeMenu").hide();
        }
    });

    $("#closeMenu").click(function(){
        $(".wrap-childmenu").css("display","none");
        $("#goMenu").show();
        $("#closeMenu").hide();
    });

    $("#goRegister").click(function() {
        $(".wrap-login").hide();
        $(".wrap-reset").hide();
        $(".wrap-register").hide().fadeIn();
    });

    $("#goLogin").click(function() {
        $(".wrap-register").hide();
        $(".wrap-reset").hide();
        $(".wrap-login").hide().fadeIn();
    });

    $("#goReset").click(function () {
        $(".wrap-login").hide();
        $(".wrap-register").hide();
        $(".wrap-reset").hide().fadeIn();
    });
});


$(window).load(function() {
    // Show spinner
    //$(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');
    
    // Show next page
    $("#wrap-merek-model").load("mulmut/brand_step", function(){
        $(".loading").html('<div class="loading"></div>');
        $("#wrap-kota").load("mulmut/city_step", function(){
        }).hide().show();
    }).hide().show();
    

    /***************** datepicker ******************/
    $('#showdatedesk.input-group.date').datepicker({
        todayHighlight: true,
        startDate: "today",
        autoclose: true,
        clearBtn: true,
        format: 'dd/mm/yyyy',
    });

    /***************** Choose Brand *************************/
    $("body").on("change", ".brand-group .next-step", function(e) {
        
        // Hide page
        $(".merek-model-step1").css("display", "none");
        $(".brand-group input[type='radio']").removeClass("next-step");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');
        
        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.brand = $(this).attr("brand");
        step.brand_id = this.value;

        // Show next page
        $("#wrap-merek-model").load("mulmut/model_step", {
            merek: step.brand_id
        }, function(){
            $(".loading").html('<div class="loading"></div>');
        }).hide().show();

    });

    /***************** Back Button Model *************************/
    $("body").on("click", "#back-button-model", function(e) {
        
        // Hide page
        $(".merek-model-step2").css("display", "none");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Show next page
        $("#wrap-merek-model").load("mulmut/brand_step", function(){
            $(".loading").html('<div class="loading"></div>');
        }).hide().show();
    });

    /***************** Choose Model *************************/
    $("body").on("change", ".model-group .next-step", function(e) {

        // Hide page
        $(".merek-model-step2").css("display", "none");
        $(".model-group input[type='radio']").removeClass("next-step");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.model_id = this.value;
        step.model = $(this).attr("model");

        /**CHANGE WITH NEW CITY**/
        $("#wrap-kota").load("mulmut/city_step", function(){
            // Hide spinner
            $(".loading").html('<div class="loading"></div>');

            //Show city
            $(".tipe-city-step").css("display", "block");

            //Show step city
            $("#wizard-p-2").css("display", "block");
        }).show();

        /*$.ajax({
            url: 'mulmut/get_number_of_transmission',
            type: 'POST',
            data: {
                model: step.model_id
            },
            success: function(data) {

                var obj = jQuery.parseJSON(data);
                // skip transmission if only one
                if (obj.no_of_transmission == 1) {
                    step.transmission = obj.transmission;
                    //skip fuel if only one
                    $.ajax({
                        url: 'mulmut/get_number_of_fuel',
                        type: 'POST',
                        data: {
                            model: step.model_id,
                            transmission: step.transmission
                        },
                        success: function(data) {
                            var obj = jQuery.parseJSON(data);
                            // skip fuel if only one
                            if (obj.no_of_fuel == 1) {

                                // Store values
                                step.fuel = obj.fuel;

                                // Show next page                               
                                $("#wrap-tipe-warna").load("mulmut/type_step", {
                                    model: step.model_id,
                                    merek: step.brand_id,
                                    transmission: step.transmission,
                                    fuel: step.fuel
                                }, function(){
                                    // Hide spinner
                                    $(".loading").html('<div class="loading"></div>');

                                    // Update bubbles
                                    $("#wizard").steps("next");
                                    $(".model-group input[type='radio']").addClass("next-step");
                                }).hide().show();
                                
                                } else {

                                // Show next page
                                $("#wrap-tipe-warna").load("mulmut/fuel_step", {
                                    model: step.model_id,
                                    merek: step.brand_id,
                                    transmission: step.transmission
                                }, function(){
                                    // Hide spinner
                                    $(".loading").html('<div class="loading"></div>');

                                    // Update bubbles
                                    $("#wizard").steps("next");
                                    $(".model-group input[type='radio']").addClass("next-step");
                                }).hide().show();

                            }
                        }
                    });

                } else {
                    // Show next page
                    $("#wrap-tipe-warna").load("mulmut/transmission_step", {
                        model: step.model_id,
                        merek: step.brand_id
                    }, function(){
                        // Hide spinner
                        $(".loading").html('<div class="loading"></div>');

                        // Update bubbles
                        $("#wizard").steps("next");
                        $(".model-group input[type='radio']").addClass("next-step");
                    }).hide().show();

                }
            }
        });*/
    });

    /***************** Back Button Transmission *************************/
    $("body").on("click", "#back-button-transmission", function(e) {
        // Hide page
        $(".tipe-warna-step1").css("display", "none");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});
        
        // Show next page
        $("#wrap-merek-model").load("mulmut/model_step", {
            merek: step.brand_id
        }, function(){
            // Hide spinner
            $(".loading").html('<div class="loading"></div>');

            // Update bubbles
            $("#wizard").steps("previous");
            $(".wizard > .steps ul>li.current").nextAll().removeClass("done");
        }).hide().show();
    });

    /***************** Choose Transmission *************************/

    $("body").on("change", ".transmission-group .next-step", function(e) {
        
        // Hide page
        $(".tipe-warna-step1").css("display", "none");
        $(".transmission-group input[type='radio']").removeClass("next-step");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.transmission = this.value;

        $.ajax({
            url: 'mulmut/get_number_of_fuel',
            type: 'POST',
            data: {
                model: step.model_id,
                transmission: step.transmission
            },
            success: function(data) {
                var obj = jQuery.parseJSON(data);
                // skip fuel if only one
                if (obj.no_of_fuel == 1) {

                    // Store values
                    step.fuel = obj.fuel;

                    // Show next page
                    $("#wrap-tipe-warna").load("mulmut/type_step", {
                        model: step.model_id,
                        merek: step.brand_id,
                        transmission: step.transmission,
                        fuel: step.fuel
                    }, function(){
                        // Hide spinner
                        $(".loading").html('<div class="loading"></div>');
                    }).hide().show();

                } else {
                    
                    // Show next page
                    $("#wrap-tipe-warna").load("mulmut/fuel_step", {
                        model: step.model_id,
                        merek: step.brand_id,
                        transmission: step.transmission
                    }, function(){
                        // Hide spinner
                        $(".loading").html('<div class="loading"></div>');
                    }).hide().show();

                }
            }
        });
    });


    /***************** Back Button Fuel *************************/
    $("body").on("click", "#back-button-fuel", function(e) {
        
        // Hide page
        $(".tipe-warna-step2").css("display", "none");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        $.ajax({
            url: 'mulmut/get_number_of_transmission',
            type: 'POST',
            data: {
                model: step.model_id
            },
            success: function(data) {
                var obj = jQuery.parseJSON(data);

                // skip transmission if only one
                if (obj.no_of_transmission == 1) {
                    
                    // Store values
                    step.model_id = "";
                    step.model = "";

                    // Show next page
                    $("#wrap-merek-model").load("mulmut/model_step", {
                        merek: step.brand_id
                    }, function(){
                        // Hide spinner
                        $(".loading").html('<div class="loading"></div>');

                        // Update bubbles
                        $("#wizard").steps("previous");
                        $("#wrap-merek-model label").removeClass("active");
                        $(".wizard > .steps ul>li.current").nextAll().removeClass("done");
                    }).hide().show();

                } else {

                    // Show next page
                    $("#wrap-tipe-warna").load("mulmut/transmission_step", {
                        model: step.model_id,
                        merek: step.brand_id
                    }, function(){
                        // Hide spinner
                        $(".loading").html('<div class="loading"></div>');
                    }).hide().show();
                }
            }
        });
    });

    /***************** Choose Fuel *************************/

    $("body").on("change", ".fuel-group .next-step", function(e) {
        // Hide page
        $(".tipe-warna-step2").css("display", "none");
        $(".fuel-group input[type='radio']").removeClass("next-step");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.fuel = this.value;
        
        // Show next page
        $("#wrap-tipe-warna").load("mulmut/type_step", {
            model: step.model_id,
            merek: step.brand_id,
            transmission: step.transmission,
            fuel: step.fuel
        }, function(){
            // Hide spinner
            $(".loading").html('<div class="loading"></div>');
        }).hide().show();
    });

    /***************** Back Button Type *************************/
    $("body").on("click", "#back-button-type", function(e) {
        // Hide page
        $(".tipe-warna-step3").css("display", "none");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.fuel = this.value;
        
        $.ajax({
            url: 'mulmut/get_number_of_fuel',
            type: 'POST',
            data: {
                model: step.model_id,
                transmission: step.transmission
            },
            success: function(data) {
                var obj = jQuery.parseJSON(data);
                // skip fuel if only one
                if (obj.no_of_fuel == 1) {
                    $.ajax({
                        url: 'mulmut/get_number_of_transmission',
                        type: 'POST',
                        data: {
                            model: step.model_id
                        },
                        success: function(data) {
                            var obj = jQuery.parseJSON(data);
                            // skip transmission if only one
                            if (obj.no_of_transmission == 1) {

                                // Store values
                                step.model_id = "";
                                step.model = "";
                                
                                // Show next page
                                $("#wrap-merek-model").load("mulmut/model_step", {
                                    merek: step.brand_id
                                }, function(){
                                    // Hide spinner
                                    $(".loading").html('<div class="loading"></div>');

                                    // Update bubbles
                                    $("#wizard").steps("previous");
                                    $("#wrap-merek-model label").removeClass("active");
                                    $(".wizard > .steps ul>li.current").nextAll().removeClass("done");
                                }).hide().show();

                            } else {
                                
                                // Show next page
                                $("#wrap-tipe-warna").load("mulmut/transmission_step", {
                                    model: step.model_id,
                                    merek: step.brand_id
                                }, function(){
                                    // Hide spinner
                                    $(".loading").html('<div class="loading"></div>');
                                }).hide().show();

                            }
                        }
                    });
                } else {

                    // Show next page
                    $("#wrap-tipe-warna").load("mulmut/fuel_step", {
                        model: step.model_id,
                        merek: step.brand_id,
                        transmission: step.transmission
                    }, function(){
                        // Hide spinner
                        $(".loading").html('<div class="loading"></div>');
                    }).hide().show();
                }
            }
        });
    });

    /***************** Choose Type *************************/

    $("body").on("click", "#tabel-tipemobil tr", function(e) {
        // Hide page
        $(".tipe-warna-step3").css("display", "none");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.type_id = $(this).children().first().html();
        step.type = $(this).find(".type-td").html();
        step.msrp = $(this).find(".type-td").attr('msrp');
        
        // Show next page
        $("#wrap-tipe-warna").load("mulmut/color_step", {
            model: step.model_id,
            merek: step.brand_id,
            transmission: step.transmission,
            fuel: step.fuel,
            type: step.type_id,
            msrp: step.msrp
        }, function(){
            // Hide spinner
            $(".loading").html('<div class="loading"></div>');
        }).hide().show();
    });

    /***************** Back Button Color *************************/
    $("body").on("click", "#back-button-color", function(e) {
        
        // Hide page
        $(".tipe-warna-step4").css("display", "none");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Show next page
        $("#wrap-tipe-warna").load("mulmut/type_step", {
            model: step.model_id,
            merek: step.brand_id,
            transmission: step.transmission,
            fuel: step.fuel
        }, function(){
            // Hide spinner
            $(".loading").html('<div class="loading"></div>');
        }).hide().show();
    });

    /***************** Choose Color *************************/

    $("body").on("click", "#table-color-choosen tr.color-tr", function(e) {

        // Hide page
        $(".tipe-warna-step4").css("display", "none");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.color_id = $(this).children().first().html();
        step.color = $(this).find(".color-td").html();
        step.color_price = $(this).find(".color-price").attr('color-price');

        // Show next page
        $("#kota-step-paragraph1").html(step.brand + " " + step.type + " (" + step.transmission +
            ", " + step.fuel + "), " + step.color);
        if (step.color_price == 0 || step.color_price.length == 0){
            $("#kota-step-paragraph2").html(rupiah(step.msrp));
        } else {
            $("#kota-step-paragraph2").html(rupiah(step.msrp) + " + " + rupiah(step.color_price));
        }

        // Update bubbles
        $("#wizard").steps("next");
        $(".tipe-city-step").css("display", "block");

        // Hide spinner
        $(".loading").html('<div class="loading"></div>');
    });

    /***************** Back Button City *************************/
    $("body").on("click", "#back-button-city", function(e) {

        // Hide page
        $(".tipe-city-step").css("display", "none");

        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Update bubbles
        $("#wrap-merek-model").load("mulmut/model_step", {
            merek: step.brand_id
        }, function(){
            // Hide spinner
            $(".loading").html('<div class="loading"></div>');

            // Update bubbles
            $("#wizard").steps("previous");
            $(".wizard > .steps ul>li.current").nextAll().removeClass("done");
        }).hide().show();

    });

    /***************** Choose City *************************/
    $("body").on("change", '.kota-group .next-step', function(e) {

        // Hide page
        $(".tipe-city-step").css("display", "none");
        
        // Show spinner
        $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');

        // Scroll to top
        $('html,body').animate({scrollTop: $("#order").offset().top});

        // Store values
        step.city_id = this.value;
        step.city = $(this).attr("city");

        $.ajax({
            url: 'order/step_2',
            type: 'GET',
            data: {
                brand: step.brand, model:step.model, city:step.city 
            },
            success: function(data) {
                window.location.href = 'order/step_3';
            }
        });
    });

    /***************** Back Button Reason *************************/
    $("body").on("click", "#back-button-message", function(e) {
        step.reason = $('input[name="alasan"]:checked').val();
        step.pref = $('input[name="pref"]:checked').val();
        step.help = $('input[name="bantuan"]:checked').val();
        step.requirementNumber = $("#requirementNumber").val();
        step.usedcar = $("#usedcar").val();
        $(".pesan-step1").css("display", "none");
        $("#wizard").steps("previous");
        $("#wrap-kota label").removeClass("active");
        $(".wizard > .steps ul>li.current").nextAll().removeClass("done");
        $('html,body').animate({scrollTop: $("#order").offset().top});
    });

    /***************** Back Button Payment *************************/
    $("body").on("click", "#back-button-payment", function(e) {
        step.term = $('#term-step').val();
        step.dpkredit = $('#dpkredit').val();
        step.termdp = $('#termdp').val();
        step.nominaldp = $('#nominalDP').val();
        step.payment = $('input[name="metodebayar"]:checked').val();
        step.estimasi_cicilan = $('#estimasi-cicilan').val();
        $(".pesan-step2").css("display", "none");
        $("#wrap-pesan").load("mulmut/reason_step", {
            reason: step.reason
        }, function() {
            $("#pesan-step-paragraph").html(step.brand + " " + step.type + " (" + step.transmission +
                ", " + step.fuel + "), " + step.color + " di " + step.city);
            if(step.color_price == 0 || step.color_price.length == 0){
                $("#pesan-step-paragraph2").html(rupiah(step.msrp));
            }else{
                $("#pesan-step-paragraph2").html(rupiah(step.msrp) + " + " + rupiah(step.color_price));
            }
        }).hide().fadeIn();
        $('html,body').animate({scrollTop: $("#order").offset().top});
    });

    /***************** Back Button Confirmation *************************/
    $("body").on("click", "#back-button-confirmation", function(e) {
        //$(".pesan-step1").css("display", "block");
        $(".tipe-city-step").css("display", "block");
        $("input[name='kotamobil']").prop('checked', false);
        $("#wizard").steps("previous");
        $(".wizard > .steps ul>li.current").nextAll().removeClass("done");
        $('html,body').animate({scrollTop: $("#order").offset().top});
    });

    /***************** Identification Card Step *************************/
    $("body").on("click", "#not-upload-id-step", function(e) {
        $(".konfirmasi-step2-2").css("display", "none");
        $("#wrap-konfirmasi").load("mulmut/last_step", {
            model: step.model_id,
            merek: step.brand_id,
            transmission: step.transmission,
            fuel: step.fuel,
            type: step.type_id,
            color: step.color_id,
            city: step.city_id,
            msrp: step.msrp,
            color_price: step.color_price
        }).hide().fadeIn();
        $('html,body').animate({scrollTop: $("#order").offset().top});
    });
});

/***************** Flexsliders ******************/
$(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: false
    });

    $('#portfolioSlider').flexslider({
        animation: "slide",
        directionNav: false,
        controlNav: true,
        touch: false,
        pauseOnHover: true,
        start: function() {
            $.waypoints('refresh');
        }
    });

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
            },
            600: {
                items: 1,
                nav: false,
            },
            1000: {
                items: 3,
                nav: true,
            }
        }
    })
});


/***************** fancybox ******************/

$(document).ready(function() {
    $(".fancybox").fancybox({
        arrows: true,
        padding: 0,
        closeBtn: true,
        openEffect: 'none',
        closeEffect: 'none',
        prevEffect: 'none',
        nextEffect: 'none',
        helpers: {
            media: {},
            overlay: {
                locked: false
            },
            buttons: false,
            thumbs: false,
            title: {
                type: 'inside'
            }
        },
        afterLoad: function() {
            this.title = '<h1 class="title-cap">' + this.title + '</h1>' + $(this.element).attr('alt');
        }
    });
});

/***************** Steps ******************/
$(function() {
    $("#wizard").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "none",
        enableKeyNavigation: false,
        enableAllSteps: false,
        enablePagination: false,
        onStepChanged: function(event, currentIndex, priorIndex) {
            $(".wizard > .steps ul>li.current").nextAll().addClass("disabled").attr("aria-disabled", "true").removeAttr("aria-selected");
        }
    });
});